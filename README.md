# CI&T Challenge API's

A project for CI&T challenges

## Technologies used
* JAVA 11
* Spring boot 2.3.3
* Cache(Caffeine)
* Mongo 4.x
* Maven
* Junit 5
* ELK stack for logs
* Docker
* Docker-compose
* Swagger
* Wiremock

## How to Run

 `cd ./client-api`
 `./mvnw clean package` 
 `cd ./transaction-api/`
 `./mvnw clean package`
 `docker-compose build`
 `docker-compose up -d`
 
* You can edit the docker-compose yml and add a new-relic license key to see/monitoring the api at newrelic.

* You can see logs on a local kibana at http://localhost:5601 just need to create an index on kibana for be able to 
  look at the logs.

## Considerations
* For be easy I am using only one database for both applications when running local