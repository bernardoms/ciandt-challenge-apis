package com.bernardoms.clientapi.controller;

import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import com.bernardoms.clientapi.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/v1/cliente")
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> saveClient(@RequestBody @Validated ClientDTO clientDTO) throws ClientAlreadyExistException {
        var uri = ServletUriComponentsBuilder.fromCurrentRequestUri().build().toUri();
        var createdClient = clientService.createClient(clientDTO);
        return ResponseEntity.created(URI.create(uri.toASCIIString() + "/" + createdClient)).build();
    }

    @PutMapping("/atualizaSaldo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBalance(@RequestBody ClientUpdateBalanceRequest clientUpdateBalanceRequest) throws AccountBalanceException, ClientNotFoundException {
        clientService.updateBalance(clientUpdateBalanceRequest);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<ClientDTO> getClients(Pageable pageRequest) {
        return clientService.getClients(pageRequest);
    }

    @GetMapping("/{cpf}")
    @ResponseStatus(HttpStatus.OK)
    public ClientDTO getClient(@PathVariable String cpf) throws ClientNotFoundException {
        return clientService.getClient(cpf);
    }
}
