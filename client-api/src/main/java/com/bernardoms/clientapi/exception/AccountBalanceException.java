package com.bernardoms.clientapi.exception;

public class AccountBalanceException extends Exception {
    public AccountBalanceException(String message) {
        super(message);
    }
}
