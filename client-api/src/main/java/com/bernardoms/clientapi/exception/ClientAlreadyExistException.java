package com.bernardoms.clientapi.exception;

public class ClientAlreadyExistException extends Exception {
    public ClientAlreadyExistException(String message) {
        super(message);
    }
}
