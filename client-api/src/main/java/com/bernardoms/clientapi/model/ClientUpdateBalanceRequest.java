package com.bernardoms.clientapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientUpdateBalanceRequest {
    @CPF
    private String cpf;
    @NotNull
    private BigDecimal value;
    @NotNull
    private OperationType operationType;
}
