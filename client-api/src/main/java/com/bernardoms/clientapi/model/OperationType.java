package com.bernardoms.clientapi.model;

public enum OperationType {
    DEBIT, CREDIT
}
