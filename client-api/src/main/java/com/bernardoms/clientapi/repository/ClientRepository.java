package com.bernardoms.clientapi.repository;

import com.bernardoms.clientapi.model.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepository extends PagingAndSortingRepository<Client, String> {
}
