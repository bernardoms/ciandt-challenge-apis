package com.bernardoms.clientapi.service;

import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClientService {
    String createClient(ClientDTO clientDTO) throws ClientAlreadyExistException;
    void updateBalance(ClientUpdateBalanceRequest clientUpdateBalanceRequest) throws ClientNotFoundException, AccountBalanceException;
    ClientDTO getClient(String cpf) throws ClientNotFoundException;
    Page<ClientDTO> getClients(Pageable pageable);
}
