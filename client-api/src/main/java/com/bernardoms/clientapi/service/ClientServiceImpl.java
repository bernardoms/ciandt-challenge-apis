package com.bernardoms.clientapi.service;

import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.model.Account;
import com.bernardoms.clientapi.model.Client;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import com.bernardoms.clientapi.model.OperationType;
import com.bernardoms.clientapi.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final ModelMapper modelMapper;

    public String createClient(ClientDTO clientDTO) throws ClientAlreadyExistException {
        checkIfIsAlreadyClient(clientDTO);

        var client = modelMapper.map(clientDTO, Client.class);

        client.setAccount(Account.builder().accountNumber(String.valueOf(UUID.randomUUID())).balance(BigDecimal.ZERO).build());

        return clientRepository.save(client).getCpf();
    }

    public void updateBalance(ClientUpdateBalanceRequest clientUpdateBalanceRequest) throws ClientNotFoundException, AccountBalanceException {
        var client = findClient(clientUpdateBalanceRequest.getCpf());

        if (OperationType.DEBIT.equals(clientUpdateBalanceRequest.getOperationType()) &&
                isNegativeValue(client.getAccount().getBalance().subtract(clientUpdateBalanceRequest.getValue()))) {
            throw new AccountBalanceException("you can't debit " + clientUpdateBalanceRequest.getValue() + " from "
                    + clientUpdateBalanceRequest.getCpf() + ", account will be negative");
        } else if (OperationType.DEBIT.equals(clientUpdateBalanceRequest.getOperationType())) {
            client.getAccount().setBalance(client.getAccount().getBalance().subtract(clientUpdateBalanceRequest.getValue()));
        } else {
            client.getAccount().setBalance(client.getAccount().getBalance().add(clientUpdateBalanceRequest.getValue()));
        }

        clientRepository.save(client);
    }

    public ClientDTO getClient(String cpf) throws ClientNotFoundException {
        return modelMapper.map(findClient(cpf), ClientDTO.class);
    }

    public Page<ClientDTO> getClients(Pageable pageable) {
        return clientRepository.findAll(pageable).map(c -> ClientDTO.builder().cpf(c.getCpf()).name(c.getName()).build());
    }

    private Client findClient(String cpf) throws ClientNotFoundException {
        return clientRepository.findById(cpf)
                .orElseThrow(() -> new ClientNotFoundException("client with cpf " + cpf + " not found!"));
    }

    private void checkIfIsAlreadyClient(ClientDTO clientDTO) throws ClientAlreadyExistException {
        if (clientRepository.findById(clientDTO.getCpf()).isPresent()) {
            throw new ClientAlreadyExistException("client with cpf " + clientDTO.getCpf() + " already exist!");
        }
    }

    private boolean isNegativeValue(BigDecimal bigDecimal) {
        return bigDecimal.signum() == -1;
    }
}
