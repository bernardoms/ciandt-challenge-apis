package com.bernardoms.clientapi.integration;

import com.bernardoms.clientapi.model.Account;
import com.bernardoms.clientapi.model.Client;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.math.BigDecimal;

public abstract class IntegrationTest {

    private static boolean alreadySaved = false;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp() {

        if (alreadySaved) {
            return;
        }
        mongoTemplate
                .save(Client
                        .builder()
                        .name("test1")
                        .cpf("12345678911")
                        .account(Account.builder()
                                .balance(BigDecimal.ZERO)
                                .accountNumber("8d864fdf-ae2d-4351-bdf9-e38af5cb2e0b")
                                .build()).build());
        mongoTemplate
                .save(Client
                        .builder()
                        .name("test2")
                        .cpf("12345678912")
                        .account(Account.builder()
                                .balance(BigDecimal.valueOf(500))
                                .accountNumber("8d864fdf-ae2d-4351-bdf9-e38af5cb2e1c")
                                .build()).build());

        mongoTemplate
                .save(Client
                        .builder()
                        .name("test3")
                        .cpf("12345678913")
                        .account(Account.builder()
                                .balance(BigDecimal.valueOf(100))
                                .accountNumber("8d864fdf-ae2d-4351-bdf9-e38af5cb2e4d")
                                .build()).build());

        alreadySaved = true;
    }
}
