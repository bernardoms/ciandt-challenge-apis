package com.bernardoms.clientapi.integration.controller;

import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.integration.IntegrationTest;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import com.bernardoms.clientapi.model.OperationType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerIntegrationTest extends IntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String URL_PATH = "/v1/cliente";

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void should_return_created_and_create_new_client() throws Exception {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("52326497015").build();

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(header().stringValues("location", "http://localhost/v1/cliente/52326497015"));
    }

    @Test
    void should_return_bad_request_when_there_is_a_validation_error() throws Exception {
        var clientDTO = ClientDTO.builder().cpf("invalid-cpf").build();

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.cpf", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.name", is("must not be blank")));
    }

    @Test
    void should_return_conflict_when_client_already_exist() throws Exception {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("52326497015").build();

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict())
                .andExpect(jsonPath("$.description", is("client with cpf 52326497015 already exist!")));
    }

    @Test
    void should_return_no_content_when_update_account_balance() throws Exception {
        var updateBalanceRequestDTO = ClientUpdateBalanceRequest.builder()
                .cpf("12345678912")
                .value(BigDecimal.valueOf(100))
                .build();

        mockMvc.perform(put(URL_PATH + "/atualizaSaldo").content(mapper.writeValueAsString(updateBalanceRequestDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isNoContent());
    }

    @Test
    void should_return_conflict_when_there_is_balance_exception() throws Exception {
        var updateBalanceRequestDTO = ClientUpdateBalanceRequest.builder()
                .cpf("12345678911")
                .operationType(OperationType.DEBIT)
                .value(BigDecimal.valueOf(100))
                .build();

        mockMvc.perform(put(URL_PATH + "/atualizaSaldo").content(mapper.writeValueAsString(updateBalanceRequestDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict())
                .andExpect(jsonPath("$.description", is("you can't debit 100 from 12345678911, account will be negative")));
    }

    @Test
    void should_return_client() throws Exception {

        mockMvc.perform(get(URL_PATH + "/12345678913")).andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf", is("12345678913")))
                .andExpect(jsonPath("$.name", is("test3")));
    }

    @Test
    void should_return_not_found_when_get_non_existent_client() throws Exception {

        mockMvc.perform(get(URL_PATH + "/52326497045")).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.description", is("client with cpf 52326497045 not found!")));
    }

    @Test
    void should_return_pageable_clients() throws Exception {

        mockMvc.perform(get(URL_PATH).param("page", "0").param("size", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name", is("test1")))
                .andExpect(jsonPath("$.totalElements", is(4)));
    }

}
