package com.bernardoms.clientapi.unit.controller;

import com.bernardoms.clientapi.controller.ClientController;
import com.bernardoms.clientapi.controller.ExceptionController;
import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import com.bernardoms.clientapi.service.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class ClientControllerUnitTest {
    private MockMvc mockMvc;
    @InjectMocks
    private ClientController clientController;
    @Mock
    private ClientService clientService;
    private static final String URL_PATH = "/v1/cliente";
    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(clientController)
                .setControllerAdvice(ExceptionController.class)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void should_return_created_and_create_new_client() throws Exception {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("52326497015").build();

        when(clientService.createClient(clientDTO)).thenReturn("52326497015");

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(header().stringValues("location", "http://localhost/v1/cliente/52326497015"));
    }

    @Test
    void should_return_bad_request_when_there_is_a_validation_error() throws Exception {
        var clientDTO = ClientDTO.builder().cpf("invalid-cpf").build();

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.cpf", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.name", is("must not be blank")));
    }

    @Test
    void should_return_conflict_when_client_already_exist() throws Exception {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("52326497015").build();

        when(clientService.createClient(clientDTO)).thenThrow(new ClientAlreadyExistException("client already exist exception"));

        mockMvc.perform(post(URL_PATH).content(mapper.writeValueAsString(clientDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict())
                .andExpect(jsonPath("$.description", is("client already exist exception")));
    }

    @Test
    void should_return_no_content_when_update_account_balance() throws Exception {
        var updateBalanceRequestDTO = ClientUpdateBalanceRequest.builder()
                .cpf("52326497015")
                .value(BigDecimal.valueOf(100))
                .build();

        mockMvc.perform(put(URL_PATH + "/atualizaSaldo").content(mapper.writeValueAsString(updateBalanceRequestDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isNoContent());
    }

    @Test
    void should_return_conflict_when_there_is_balance_exception() throws Exception {
        var updateBalanceRequestDTO = ClientUpdateBalanceRequest.builder()
                .cpf("52326497015")
                .value(BigDecimal.valueOf(100))
                .build();
        doThrow(new AccountBalanceException("account balance exception")).when(clientService).updateBalance(updateBalanceRequestDTO);

        mockMvc.perform(put(URL_PATH + "/atualizaSaldo").content(mapper.writeValueAsString(updateBalanceRequestDTO)).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isConflict())
                .andExpect(jsonPath("$.description", is("account balance exception")));
    }

    @Test
    void should_return_client() throws Exception {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("52326497015").build();

        when(clientService.getClient("52326497015")).thenReturn(clientDTO);

        mockMvc.perform(get(URL_PATH + "/52326497015")).andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf", is("52326497015")))
                .andExpect(jsonPath("$.name", is("test1")));
    }

    @Test
    void should_return_not_found_when_get_non_existent_client() throws Exception {
        when(clientService.getClient("52326497015")).thenThrow(new ClientNotFoundException("client not found!"));

        mockMvc.perform(get(URL_PATH + "/52326497015")).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.description", is("client not found!")));
    }

    @Test
    void should_return_pageable_clients() throws Exception {
        var client1 = ClientDTO.builder()
                .name("test1")
                .cpf("12345678911").build();

        var client2 = ClientDTO.builder()
                .name("test2")
                .cpf("12345678912").build();

        when(clientService.getClients(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(client1, client2)));

        mockMvc.perform(get(URL_PATH).param("page", "0").param("size", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name", is("test1")))
                .andExpect(jsonPath("$.totalElements", is(2)));
    }
}
