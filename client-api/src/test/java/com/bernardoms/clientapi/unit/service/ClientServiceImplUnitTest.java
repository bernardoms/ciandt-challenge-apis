package com.bernardoms.clientapi.unit.service;

import com.bernardoms.clientapi.dto.ClientDTO;
import com.bernardoms.clientapi.exception.AccountBalanceException;
import com.bernardoms.clientapi.exception.ClientAlreadyExistException;
import com.bernardoms.clientapi.exception.ClientNotFoundException;
import com.bernardoms.clientapi.model.Account;
import com.bernardoms.clientapi.model.Client;
import com.bernardoms.clientapi.model.ClientUpdateBalanceRequest;
import com.bernardoms.clientapi.model.OperationType;
import com.bernardoms.clientapi.repository.ClientRepository;
import com.bernardoms.clientapi.service.ClientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientServiceImplUnitTest {
    @InjectMocks
    private ClientServiceImpl clientService;
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<Client> clientArgumentCaptor;

    @Test
    void should_create_client_with_empty_account() throws ClientAlreadyExistException {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("12345678911").build();
        var client = Client.builder()
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.ZERO)
                        .build()).build();

        when(modelMapper.map(clientDTO, Client.class)).thenReturn(client);
        when(clientRepository.save(client)).thenReturn(client);

        var clientUpdated = clientService.createClient(clientDTO);
        assertEquals("12345678911", clientUpdated);
        assertEquals(BigDecimal.ZERO, client.getAccount().getBalance());
    }

    @Test
    void should_throws_client_already_exist_when_save_a_existing_client() {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("12345678911").build();

        var client = Client.builder()
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.ZERO)
                        .build()).build();

        when(clientRepository.findById("12345678911")).thenReturn(Optional.of(client));
        var exception = assertThrows(ClientAlreadyExistException.class, () -> {
            clientService.createClient(clientDTO);
        });

        assertEquals("client with cpf 12345678911 already exist!", exception.getMessage());
    }

    @Test
    void should_throws_client_account_balance_exception_when_debit_a_value_higher_than_account_value() {
        var clientUpdateBalanceRequest = ClientUpdateBalanceRequest.builder()
                .cpf("12345678911").operationType(OperationType.DEBIT).value(BigDecimal.ONE).build();

        var client = Client.builder()
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.ZERO)
                        .build()).build();

        when(clientRepository.findById("12345678911")).thenReturn(Optional.of(client));

        var exception = assertThrows(AccountBalanceException.class, () -> {
            clientService.updateBalance(clientUpdateBalanceRequest);
        });

        assertEquals("you can't debit 1 from 12345678911, account will be negative", exception.getMessage());
    }

    @Test
    void should_debit_from_client_account_when_debit_a_value_is_lower_than_account_value() throws AccountBalanceException, ClientNotFoundException {
        var clientUpdateBalanceRequest = ClientUpdateBalanceRequest.builder()
                .cpf("12345678911").operationType(OperationType.DEBIT).value(BigDecimal.ONE).build();

        var client = Client.builder()
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.valueOf(100))
                        .build()).build();

        when(clientRepository.findById("12345678911")).thenReturn(Optional.of(client));

        clientService.updateBalance(clientUpdateBalanceRequest);

        verify(clientRepository, times(1)).save(clientArgumentCaptor.capture());

        assertEquals(BigDecimal.valueOf(99), clientArgumentCaptor.getValue().getAccount().getBalance());
    }

    @Test
    void should_gave_client_account_credit() throws AccountBalanceException, ClientNotFoundException {
        var clientUpdateBalanceRequest = ClientUpdateBalanceRequest.builder()
                .cpf("12345678911").operationType(OperationType.CREDIT).value(BigDecimal.ONE).build();

        var client = Client.builder()
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.valueOf(100))
                        .build()).build();

        when(clientRepository.findById("12345678911")).thenReturn(Optional.of(client));

        clientService.updateBalance(clientUpdateBalanceRequest);

        verify(clientRepository, times(1)).save(clientArgumentCaptor.capture());

        assertEquals(BigDecimal.valueOf(101), clientArgumentCaptor.getValue().getAccount().getBalance());
    }

    @Test
    void should_return_client() throws ClientNotFoundException {
        var clientDTO = ClientDTO.builder().name("test1")
                .cpf("12345678911").build();

        var client = Client.builder()
                .name("test1")
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.valueOf(100))
                        .build()).build();

        when(clientRepository.findById("12345678911")).thenReturn(Optional.of(client));
        when(modelMapper.map(client, ClientDTO.class)).thenReturn(clientDTO);

        var foundClient = clientService.getClient("12345678911");

        assertEquals("12345678911", foundClient.getCpf());
        assertEquals("test1", foundClient.getName());
    }

    @Test
    void should_throws_client_not_found_exception() {
        var exception = assertThrows(ClientNotFoundException.class, () -> {
            clientService.getClient("12345678911");
        });

        assertEquals("client with cpf 12345678911 not found!", exception.getMessage());
    }

    @Test
    void should_return_all_pageable_clients() {
        var client1 = Client.builder()
                .name("test1")
                .cpf("12345678911")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.valueOf(100))
                        .build()).build();

        var client2 = Client.builder()
                .name("test2")
                .cpf("12345678912")
                .account(Account.builder()
                        .accountNumber(UUID.randomUUID().toString())
                        .balance(BigDecimal.valueOf(0))
                        .build()).build();

        when(clientRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(client1, client2)));

        var clients = clientService.getClients(Pageable.unpaged());

        assertEquals(2, clients.getTotalElements());

        assertEquals(1, clients.getTotalPages());

    }
}
