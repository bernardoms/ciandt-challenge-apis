package com.bernardoms.transactionapi.controller;

import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.model.AddBalanceRequest;
import com.bernardoms.transactionapi.model.TransactionResponse;
import com.bernardoms.transactionapi.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;

    @GetMapping("/saldo/{cpf}")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse getClientTransactions(@PathVariable String cpf) {
        return transactionService.getClientTransactions(cpf);
    }

    @PostMapping("/addSaldo")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addBalance(@RequestBody @Validated AddBalanceRequest addBalanceRequest) {
        transactionService.addBalanceTransaction(addBalanceRequest);
    }

    @PostMapping("/transfer")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void transfer(@RequestBody @Validated TransactionDTO transactionDTO) {
        transactionService.transferTransaction(transactionDTO);
    }
}
