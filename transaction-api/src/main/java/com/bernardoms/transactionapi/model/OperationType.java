package com.bernardoms.transactionapi.model;

public enum OperationType {
    DEBIT, CREDIT
}
