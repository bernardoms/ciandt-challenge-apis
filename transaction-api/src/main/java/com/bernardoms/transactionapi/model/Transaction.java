package com.bernardoms.transactionapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "transfers")
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {
    private String from;
    private String to;
    private BigDecimal value;
}
