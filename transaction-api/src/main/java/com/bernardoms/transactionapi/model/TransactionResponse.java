package com.bernardoms.transactionapi.model;

import com.bernardoms.transactionapi.dto.TransactionDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponse {
    private String name;
    private BigDecimal balance;
    private List<TransactionDTO> transactions = new ArrayList<>();
}
