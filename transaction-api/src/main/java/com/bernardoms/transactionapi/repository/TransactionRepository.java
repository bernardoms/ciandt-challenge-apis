package com.bernardoms.transactionapi.repository;

import com.bernardoms.transactionapi.model.Transaction;
import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, ObjectId> {
    List<Transaction> findAllByFrom(String from);
}
