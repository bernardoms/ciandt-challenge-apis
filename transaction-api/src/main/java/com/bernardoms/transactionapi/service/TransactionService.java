package com.bernardoms.transactionapi.service;

import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.model.AddBalanceRequest;
import com.bernardoms.transactionapi.model.TransactionResponse;

public interface TransactionService {
    TransactionResponse getClientTransactions(String cpf);

    void transferTransaction(TransactionDTO transactionDTO);

    void addBalanceTransaction(AddBalanceRequest addBalanceRequest);
}
