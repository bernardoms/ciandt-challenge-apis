package com.bernardoms.transactionapi.service;

import com.bernardoms.transactionapi.config.ClientApiConfig;
import com.bernardoms.transactionapi.model.*;
import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final ClientApiConfig clientApiConfig;
    private final RestTemplate restTemplate;
    private final TransactionRepository transactionRepository;


    public TransactionResponse getClientTransactions(String cpf) {
        var client = getClients(cpf);

        var transactions = transactionRepository.findAllByFrom(cpf);

        var transactionsDTO = transactions.stream().map(t -> TransactionDTO.builder()
                .fromCPF(t.getFrom())
                .toCPF(t.getTo())
                .value(t.getValue())
                .build()).collect(Collectors.toList());

        return TransactionResponse.builder().balance(client.getAccount().getBalance())
                .name(client.getName())
                .transactions(transactionsDTO).build();
    }

    public void transferTransaction(TransactionDTO transactionDTO) {

        getClients(transactionDTO.getFromCPF());
        getClients(transactionDTO.getToCPF());

        var fromUpdate = UpdateBalanceRequest.builder()
                .cpf(transactionDTO.getFromCPF())
                .operationType(OperationType.DEBIT)
                .value(transactionDTO.getValue())
                .build();

        var toUpdate = UpdateBalanceRequest.builder()
                .cpf(transactionDTO.getToCPF())
                .operationType(OperationType.CREDIT)
                .value(transactionDTO.getValue())
                .build();

        updateClientBalance(fromUpdate);

        try {
            updateClientBalance(toUpdate);
        } catch (HttpClientErrorException | HttpServerErrorException e) {

            var rollbackFromUpdate = UpdateBalanceRequest.builder()
                    .cpf(transactionDTO.getFromCPF())
                    .operationType(OperationType.CREDIT)
                    .value(transactionDTO.getValue())
                    .build();
            updateClientBalance(rollbackFromUpdate);

            throw new HttpServerErrorException(e.getStatusCode());
        }

        var transaction = Transaction.builder()
                .from(transactionDTO.getFromCPF())
                .to(transactionDTO.getToCPF())
                .value(transactionDTO.getValue().negate())
                .build();

        transactionRepository.save(transaction);
    }

    public void addBalanceTransaction(AddBalanceRequest addBalanceRequest) {
        var updateBalance = UpdateBalanceRequest.builder()
                .cpf(addBalanceRequest.getCpf())
                .operationType(OperationType.CREDIT)
                .value(addBalanceRequest.getValue())
                .build();

        updateClientBalance(updateBalance);

        var transaction = Transaction.builder()
                .from(addBalanceRequest.getCpf())
                .to(addBalanceRequest.getCpf())
                .value(addBalanceRequest.getValue())
                .build();

        transactionRepository.save(transaction);
    }

    private void updateClientBalance(UpdateBalanceRequest updateBalanceRequest) {
        var request =
                new HttpEntity<>(updateBalanceRequest, null);

        restTemplate.put(clientApiConfig.getEndpoint() + "/atualizaSaldo", request);
    }

    private Client getClients(String cpf) {
        return restTemplate.getForObject(clientApiConfig.getEndpoint() + "/" + cpf, Client.class);
    }
}
