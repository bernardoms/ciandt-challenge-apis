package com.bernardoms.transactionapi.integration;

import com.bernardoms.transactionapi.model.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.math.BigDecimal;

public abstract class IntegrationTest {

    private static boolean alreadySaved = false;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp() {

        if (alreadySaved) {
            return;
        }
        mongoTemplate
                .save(Transaction
                        .builder()
                        .value(BigDecimal.valueOf(50))
                        .to("73240294001")
                        .from("26827038017")
                        .build());

        alreadySaved = true;
    }
}
