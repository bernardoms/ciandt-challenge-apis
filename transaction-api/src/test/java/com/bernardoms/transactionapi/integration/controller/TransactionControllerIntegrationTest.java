package com.bernardoms.transactionapi.integration.controller;

import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.integration.IntegrationTest;
import com.bernardoms.transactionapi.model.AddBalanceRequest;
import com.bernardoms.transactionapi.model.TransactionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerIntegrationTest extends IntegrationTest {
    private static final String URL_PATH = "/v1";
    @Autowired
    private MockMvc mockMvc;
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void should_return_client_transactions() throws Exception {
        mockMvc.perform(get(URL_PATH + "/saldo/26827038017")).andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("teste2")))
                .andExpect(jsonPath("$.balance", is(200)))
                .andExpect(jsonPath("$.transactions[0].fromCPF", is("26827038017")))
                .andExpect(jsonPath("$.transactions[0].toCPF", is("73240294001")));
    }

    @Test
    void should_return_bad_request_when_have_validation_error_on_add_balance() throws Exception {
        var addBalanceRequest = AddBalanceRequest.builder().cpf("123").build();

        mockMvc.perform(post(URL_PATH + "/addSaldo").content(mapper.writeValueAsString(addBalanceRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.cpf", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.value", is("must not be null")));
    }

    @Test
    void should_add_balance_with_success() throws Exception {
        var addBalanceRequest = AddBalanceRequest.builder().cpf("73240294001").value(BigDecimal.valueOf(10)).build();

        mockMvc.perform(post(URL_PATH + "/addSaldo").content(mapper.writeValueAsString(addBalanceRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_bad_request_when_have_validation_error_on_transfer() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("123").toCPF("42345").build();

        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.fromCPF", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.toCPF", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.value", is("must not be null")));
    }

    @Test
    void should_transfer_with_success() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("26827038017").toCPF("73240294001").value(BigDecimal.valueOf(10)).build();

        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_not_found_when_transfer_from_a_non_existent_user() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("59082103079").toCPF("73240294001").value(BigDecimal.valueOf(10)).build();

        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
