package com.bernardoms.transactionapi.unit;

import com.bernardoms.transactionapi.controller.ExceptionController;
import com.bernardoms.transactionapi.controller.TransactionController;
import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.model.AddBalanceRequest;
import com.bernardoms.transactionapi.model.TransactionResponse;
import com.bernardoms.transactionapi.service.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpServerErrorException;

import java.math.BigDecimal;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class TransactionControllerUnitTest {
    private MockMvc mockMvc;
    @InjectMocks
    private TransactionController transactionController;
    @Mock
    private TransactionService transactionService;
    private static final String URL_PATH = "/v1";
    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(transactionController)
                .setControllerAdvice(ExceptionController.class)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void should_return_client_transactions() throws Exception {
        var transactionResponse = TransactionResponse.builder()
                .balance(BigDecimal.valueOf(100))
                .name("test1")
                .transactions(Collections.singletonList(TransactionDTO.builder().value(BigDecimal.valueOf(50)).fromCPF("543534534").toCPF("52326497015").build()))
                .build();

        when(transactionService.getClientTransactions("52326497015")).thenReturn(transactionResponse);
        mockMvc.perform(get(URL_PATH + "/saldo/52326497015")).andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("test1")))
                .andExpect(jsonPath("$.balance", is(100)))
                .andExpect(jsonPath("$.transactions[0].fromCPF", is("543534534")))
                .andExpect(jsonPath("$.transactions[0].toCPF", is("52326497015")));
    }

    @Test
    void should_return_bad_request_when_have_validation_error_on_add_balance() throws Exception {
        var addBalanceRequest = AddBalanceRequest.builder().cpf("123").build();

        mockMvc.perform(post(URL_PATH + "/addSaldo").content(mapper.writeValueAsString(addBalanceRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.cpf", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.value", is("must not be null")));
    }

    @Test
    void should_add_balance_with_success() throws Exception {
        var addBalanceRequest = AddBalanceRequest.builder().cpf("52326497015").value(BigDecimal.valueOf(10)).build();

        mockMvc.perform(post(URL_PATH + "/addSaldo").content(mapper.writeValueAsString(addBalanceRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_bad_request_when_have_validation_error_on_transfer() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("123").toCPF("42345").build();

        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description.fromCPF", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.toCPF", is("invalid Brazilian individual taxpayer registry number (CPF)")))
                .andExpect(jsonPath("$.description.value", is("must not be null")));
    }

    @Test
    void should_return_client_exception_when_client_error_happens_on_transfer() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("52326497015").toCPF("73240294001").value(BigDecimal.valueOf(10)).build();
        doThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(transactionService).transferTransaction(transferRequest);
        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void should_transfer_with_success() throws Exception {
        var transferRequest = TransactionDTO.builder().fromCPF("52326497015").toCPF("73240294001").value(BigDecimal.valueOf(10)).build();

        mockMvc.perform(post(URL_PATH + "/transfer").content(mapper.writeValueAsString(transferRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
