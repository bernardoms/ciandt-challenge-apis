package com.bernardoms.transactionapi.unit;

import com.bernardoms.transactionapi.config.ClientApiConfig;
import com.bernardoms.transactionapi.dto.TransactionDTO;
import com.bernardoms.transactionapi.model.*;
import com.bernardoms.transactionapi.repository.TransactionRepository;
import com.bernardoms.transactionapi.service.TransactionServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplUnitTest {
    @InjectMocks
    private TransactionServiceImpl transactionService;
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private ClientApiConfig clientApiConfig;
    @Captor
    private ArgumentCaptor<HttpEntity<UpdateBalanceRequest>> updateBalanceRequestCaptor;
    @Captor
    private ArgumentCaptor<Transaction> transactionArgumentCaptor;

    @Test
    void should_return_client_balance_and_transactions() {
        var client = Client.builder()
                .cpf("73240294001")
                .name("test1")
                .account(Client.Account.builder()
                        .accountNumber("1234")
                        .balance(BigDecimal.valueOf(100))
                        .build()).build();

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenReturn(client);
        when(transactionRepository.findAllByFrom("73240294001")).thenReturn(Collections.singletonList(Transaction.builder()
                .to("5464564564")
                .value(BigDecimal.valueOf(50))
                .from("73240294001")
                .build()));

        var clientTransactionResponse = transactionService.getClientTransactions("73240294001");

        assertEquals(BigDecimal.valueOf(100), clientTransactionResponse.getBalance());
        assertEquals(client.getName(), clientTransactionResponse.getName());
        assertEquals("5464564564", clientTransactionResponse.getTransactions().get(0).getToCPF());
        assertEquals("73240294001", clientTransactionResponse.getTransactions().get(0).getFromCPF());
        assertEquals(BigDecimal.valueOf(50), clientTransactionResponse.getTransactions().get(0).getValue());
    }

    @Test
    void should_do_nothing_when_client_from_not_found() {
        var transaction = TransactionDTO.builder().fromCPF("73240294001").toCPF("5464564564").value(BigDecimal.valueOf(100)).build();

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));


        assertThrows(HttpClientErrorException.class, () -> {
            transactionService.transferTransaction(transaction);
        });

        verify(restTemplate, never()).put(anyString(), any());
        verify(transactionRepository, never()).save(any());
    }

    @Test
    void should_do_nothing_when_client_to_not_found() {
        var transaction = TransactionDTO.builder().fromCPF("73240294001").toCPF("5464564564").value(BigDecimal.valueOf(100)).build();

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenReturn(Client.builder().build());
        when(restTemplate.getForObject("http://test.com/v1/cliente/5464564564", Client.class)).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));


        assertThrows(HttpClientErrorException.class, () -> {
            transactionService.transferTransaction(transaction);
        });

        verify(restTemplate, never()).put(anyString(), any());
        verify(transactionRepository, never()).save(any());
    }

    @Test
    void should_do_nothing_when_there_is_an_error_to_debit_value_from_transfer() {
        var transaction = TransactionDTO.builder().fromCPF("73240294001").toCPF("5464564564").value(BigDecimal.valueOf(100)).build();

        var updateFromRequest = UpdateBalanceRequest.builder()
                .value(BigDecimal.valueOf(100))
                .operationType(OperationType.DEBIT)
                .cpf("73240294001").build();

        var fromRequest =
                new HttpEntity<>(updateFromRequest, null);

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenReturn(Client.builder().build());
        when(restTemplate.getForObject("http://test.com/v1/cliente/5464564564", Client.class)).thenReturn(Client.builder().build());
        doThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(restTemplate).put("http://test.com/v1/cliente/atualizaSaldo", fromRequest);


        assertThrows(HttpServerErrorException.class, () -> {
            transactionService.transferTransaction(transaction);
        });

        verify(restTemplate, times(1)).put(anyString(), any());
        verify(transactionRepository, never()).save(any());
    }

    @Test
    void should_give_back_debit_value_when_a_error_happened_while_transferring() {
        var transaction = TransactionDTO.builder().fromCPF("73240294001").toCPF("5464564564").value(BigDecimal.valueOf(100)).build();


        var updateFromRequest = UpdateBalanceRequest.builder()
                .value(BigDecimal.valueOf(100))
                .operationType(OperationType.DEBIT)
                .cpf("73240294001").build();

        var fromRequest =
                new HttpEntity<>(updateFromRequest, null);

        var updateToRequest = UpdateBalanceRequest.builder()
                .value(BigDecimal.valueOf(100))
                .operationType(OperationType.CREDIT)
                .cpf("5464564564").build();

        var toRequest =
                new HttpEntity<>(updateToRequest, null);


        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenReturn(Client.builder().build());
        when(restTemplate.getForObject("http://test.com/v1/cliente/5464564564", Client.class)).thenReturn(Client.builder().build());
        doNothing().when(restTemplate).put("http://test.com/v1/cliente/atualizaSaldo", fromRequest);
        doThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(restTemplate).put("http://test.com/v1/cliente/atualizaSaldo", toRequest);


        assertThrows(HttpServerErrorException.class, () -> {
            transactionService.transferTransaction(transaction);
        });

        verify(restTemplate, times(3)).put(anyString(), updateBalanceRequestCaptor.capture());
        verify(transactionRepository, never()).save(any());
        assertEquals("73240294001", updateBalanceRequestCaptor.getAllValues().get(2).getBody().getCpf());
        assertEquals(OperationType.CREDIT, updateBalanceRequestCaptor.getAllValues().get(2).getBody().getOperationType());
        assertEquals(BigDecimal.valueOf(100), updateBalanceRequestCaptor.getAllValues().get(2).getBody().getValue());
    }

    @Test
    void should_transfer_a_value_and_save_transaction_with_success() {
        var transaction = TransactionDTO.builder().fromCPF("73240294001").toCPF("5464564564").value(BigDecimal.valueOf(100)).build();


        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");
        when(restTemplate.getForObject("http://test.com/v1/cliente/73240294001", Client.class)).thenReturn(Client.builder().build());
        when(restTemplate.getForObject("http://test.com/v1/cliente/5464564564", Client.class)).thenReturn(Client.builder().build());

        transactionService.transferTransaction(transaction);

        verify(restTemplate, times(2)).put(anyString(), updateBalanceRequestCaptor.capture());
        verify(transactionRepository, times(1)).save(transactionArgumentCaptor.capture());

        assertEquals("73240294001", updateBalanceRequestCaptor.getAllValues().get(0).getBody().getCpf());
        assertEquals(OperationType.DEBIT, updateBalanceRequestCaptor.getAllValues().get(0).getBody().getOperationType());
        assertEquals(BigDecimal.valueOf(100), updateBalanceRequestCaptor.getAllValues().get(0).getBody().getValue());

        assertEquals("5464564564", updateBalanceRequestCaptor.getAllValues().get(1).getBody().getCpf());
        assertEquals(OperationType.CREDIT, updateBalanceRequestCaptor.getAllValues().get(1).getBody().getOperationType());
        assertEquals(BigDecimal.valueOf(100), updateBalanceRequestCaptor.getAllValues().get(1).getBody().getValue());

        assertEquals("73240294001", transactionArgumentCaptor.getValue().getFrom());
        assertEquals("5464564564", transactionArgumentCaptor.getValue().getTo());
        assertEquals(BigDecimal.valueOf(100).negate(), transactionArgumentCaptor.getValue().getValue());
    }

    @Test
    void should_do_nothing_when_there_is_a_exception_to_add_balance() {
        var addBalance = AddBalanceRequest.builder().cpf("73240294001").value(BigDecimal.valueOf(100)).build();

        var updateBalanceRequest = UpdateBalanceRequest.builder()
                .value(BigDecimal.valueOf(100))
                .operationType(OperationType.CREDIT)
                .cpf("73240294001").build();

        var updateRequest =
                new HttpEntity<>(updateBalanceRequest, null);

        doThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(restTemplate).put("http://test.com/v1/cliente/atualizaSaldo", updateRequest);

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");

        assertThrows(HttpServerErrorException.class, () -> {
            transactionService.addBalanceTransaction(addBalance);
        });
        verify(transactionRepository, never()).save(any());
    }

    @Test
    void should_save_transaction_when_add_balance_with_success() {
        var addBalance = AddBalanceRequest.builder().cpf("73240294001").value(BigDecimal.valueOf(100)).build();

        when(clientApiConfig.getEndpoint()).thenReturn("http://test.com/v1/cliente");

        transactionService.addBalanceTransaction(addBalance);

        verify(transactionRepository, times(1)).save(transactionArgumentCaptor.capture());
        assertEquals("73240294001", transactionArgumentCaptor.getValue().getFrom());
        assertEquals("73240294001", transactionArgumentCaptor.getValue().getTo());
        assertEquals(BigDecimal.valueOf(100), transactionArgumentCaptor.getValue().getValue());
    }
}
